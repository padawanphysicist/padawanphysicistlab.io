.. title: Recuperando arquivos de texto com grep
.. slug: recuperando-arquivos-de-texto-com-grep
.. date: 2023-12-15 23:10:09 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Enquanto atualizava meu `currículo <https://github.com/padawanphysicist/resume>`_, excluí a biblioteca que gera a versão em pdf. Para minha surpresa, a extensão do arquivo estava dentro do escopo ``.gitignore``, então o arquivo não foi confirmado e perdi um dia de trabalho. Eu explorei algumas maneiras de recuperar arquivos, como `scalpel <https://github.com/sleuthkit/scalpel>`_ e `foremost <https://github.com/mistal-distal/foremost>`_, mas nenhum deles funcionou. Depois de perder a esperança, fiquei surpreso ao descobrir que o ``grep`` poderia me ajudar!

.. TEASER_END

Quando você exclui um arquivo, ele não é completamente removido do disco rígido, mesmo quando você tem certeza de que a lixeira foi esvaziada. O espaço em disco usado para armazenar o arquivo é simplesmente *desalocado*, o que significa que o espaço em disco agora pode ser usado para armazenar novos dados sempre que você gravar dados na unidade. Antes que esse espaço seja reutilizado, os arquivos excluídos permanentemente podem ser recuperados.

Olhando a página de manual do ``grep`` eu encontrei isto:

.. image:: /images/Screenshot_20231215_235023.png

o que significa que os dispositivos são lidos como se fossem arquivos comuns por padrão! Então, se você souber um trecho de texto para procurar, poderá encontrar o arquivo mesmo que ele tenha sido excluído!
           
Eu estava procurando por um arquivo de biblioteca Emacs Lisp chamado `ox-altacv.el`, e as bibliotecas Emacs geralmente começam com um texto do tipo

.. code::
   
   ;;; ox-altacv.el blah blah blah

A partição onde o arquivo excluído pertence era ``/dev/sdc``. Então, tentei o seguinte comando:

.. code:: bash
   
   $ sudo grep --text --after-context=300 --before-context=300 ';;; ox-altacv.el' /dev/sdc > recovery_data

Quando o comando terminar (pode demorar bastante), você terá um arquivo (possivelmente) grande contendo todo tipo de texto (ASCII e não ASCII).

Eu sabia que não havia texto não-ASCII em meu arquivo, então era inútil manter o lixo não-ASCII. Para limpar o arquivo, usei o comando ``strings``:

.. code:: bash
   
   $ strings recovery_data > data.txt

No final, ``data.txt`` continha meu script, mas também muitos outros scripts que contêm a mesma linha que eu estava tentando combinar, mas foi fácil encontrar o trecho certo.
