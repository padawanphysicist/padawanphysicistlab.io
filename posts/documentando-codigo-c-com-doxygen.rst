.. title: Documentando código C com o Doxygen
.. slug: documentando-codigo-c-com-doxygen
.. date: 2023-09-28 22:48:59 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

A documentação é, sem dúvida, uma parte fundamental no processo de
desenvolvimento de código. Ela permite que os usuários e desenvolvedores
tenham uma maior compreensão do funcionamento do código e sua
utilização, permitindo que a manutenção a médio e longo prazo seja feita
de forma mais célere e que essas pessoas possam participar também do
desenvolvimento do projeto.

.. TEASER_END

Para projetos em C/C++, o `Doxygen <https://www.doxygen.nl/>`__ é a
ferramenta mais amplamente utilizada. Um exemplo de projeto que utiliza
o Doxygen para documentação é o `KDE <https://kde.org/pt-br/>`__.

O Doxygen gera a documentação a partir do código, anotado por blocos de
comentário com `marcações
especiais <https://www.doxygen.nl/manual/docblocks.html#specialblock>`__,
e pode gerar uma saída em html e LaTeX.

Vamos aqui criar um projeto simples em C para exemplificar a produção de
documentação com o Doxygen. O projeto vai se chamar ``hello``, e contém
apenas uma função, que irá mostrar ``"Hello World"`` na tela.

Criando o código do projeto
===========================

O nosso projeto terá uma estrutura bem simples, com todos os arquivos no
mesmo diretório (que eu nomeei ``hello``):

.. code::

   mkdir hello && cd hello && touch hello.c hello.h main.c

O conteúdo do cabeçalho ``hello.h`` é

.. code:: c

   #ifndef __HELLO_H__
   #define __HELLO_H__

   #ifndef MY_CONST
   #    define MY_CONST 42
   #endif

   void
   hello (void);

   #endif /* __HELLO_H__ */

e o código da função no arquivo ``hello.c``

.. code:: c

   void
   hello (void)
   {
       printf ("Hello World\n");
   }

Para chamarmos a função ``hello``, vamos criar também o arquivo
``main.c``, que contém a função principal:

.. code:: c

   #include "hello.h"

   int main (void)
   {
       hello ();
       return MY_CONST;
   }

Para testar, podemos compilar o nosso projeto de uma forma simples no
terminal:

.. code::

   $ gcc -c hello.c 
   $ gcc -c main.c 
   $ gcc hello.o main.o -o hello.exe

Executando ``./hello.exe`` veremos a mensagem ``Hello World`` na tela:

.. image:: /images/Screenshot_20230928_231256.png

Instalando o Doxygen
====================

Você pode checar se o Doxygen está instalado através do comando:

.. code::

   $ doxygen -v

Caso não tenha instalado, você pode olhar na `página oficial do
projeto <https://www.doxygen.nl/manual/install.html>`__ ou na página da
sua distribuição: os usuários do `Gentoo <https://www.gentoo.org/>`__
(como eu) podem instalar com o
`Portage <https://wiki.gentoo.org/wiki/Portage>`__,

.. code::

   # emerge --ask app-doc/doxygen

e para usuários do
`Debian <https://www.debian.org/index.pt.html>`__/`Ubuntu <https://ubuntu.com/>`__,

.. code:: 

   # apt-get install doxygen

Produzindo a documentação
=========================

Como eu `falei anteriormente <#doxygen-how>`__, o Doxygen utiliza de
anotações especiais no código para gerar a documentação. Para o nosso
projeto, podemos reescrever o arquivo de cabeçalho ``hello.h`` da
seguinte forma:

.. code:: c

   /// @file hello.h
   #ifndef __HELLO_H__
   #define __HELLO_H__

   /**
    * The answer to the ultimate question about life, the Universe, and
    * everything.
    */
   #ifndef MY_CONST
   #    define MY_CONST 42
   #endif

   /**
    * Say hello!
    */
   void
   hello (void);

   #endif /* __HELLO_H__ */

Note a presença dos blocos

::

   /**
    * ... text ...
    */

no cabeçalho.

Criando o Doxyfile
------------------

O ``Doxyfile`` é o arquivo de configuração que controla o Doxygen. Para
criar um modelo de configuração, execute no diretório principal do
projeto:

.. code::

   $ doxygen -g

onde o ``-g`` significa ``generate``. Isso irá criar o arquivo
``Doxyfile`` no diretório do projeto.

Gerando a documentação do Doxygen
---------------------------------

Para criar a documentação com o Doxygen:

.. code::

   $ doxygen

Isto irá gerar dois novos diretórios:

-  ``html/``,
-  ``latex/``.

Por padrão, o Doxygen cria uma documentação em formato
`LaTeX <https://www.latex-project.org/>`__ além do HTML.

Se você abrir o arquivo ``html/index.html`` no seu navegador, você vai
ver algo parecido com isso:

.. image:: /images/Screenshot_20230928_235627.png 
   :width: 85%

E entrando no menu ``Files > File List > hello.h``, você verá a
documentação do arquivo ``hello.h``:

.. image:: /images/Screenshot_20230928_235317.png        

Melhorando a cara da documentação
=================================

Embora o resultado seja funcional, a página gerada possui um visual bem
básico. Para tornar a aparência um pouco mais atrativa, o Doxygen
oferece algumas `opções de
customização <https://www.doxygen.nl/manual/customize.html>`__, e entre
elas, a opção de incluir estilos em CSS.

É aí que entra o projeto `Doxygen
Awesome <https://jothepro.github.io/doxygen-awesome-css/>`__, que
fornece uma camada de estilo adicional para o Doxygen.

Para utilizá-lo existem algumas opções de instalação na `página
oficial <https://jothepro.github.io/doxygen-awesome-css/index.html#autotoc_md10>`__,
e eu vou seguir com a instalação manual, clonando o repositório como um
submódulo do git:

.. code::

   git submodule add https://github.com/jothepro/doxygen-awesome-css.git
   cd doxygen-awesome-css
   git checkout v2.2.1

O projeto oferece dois layouts, e vou escolher o layout com barra
lateral. Para isso é também necessário modificar as seguintes opções do
``Doxyfile``:

.. code::

   GENERATE_TREEVIEW      = YES # required!
   DISABLE_INDEX          = NO
   FULL_SIDEBAR           = NO
   HTML_EXTRA_STYLESHEET  = doxygen-awesome-css/doxygen-awesome.css \
                            doxygen-awesome-css/doxygen-awesome-sidebar-only.css
   HTML_COLORSTYLE        = LIGHT # required with Doxygen >= 1.9.5

E, invocando ``doxygen`` novamente, a documentação gerada será parecida
com esta:

.. image:: /images/Screenshot_20230929_002215.png
   :width: 85%
   :alt: alternate text
         
Código de exemplo
=================

O repositório contendo o código utilizado como exemplo está disponível em:

https://github.com/padawanphysicist/c-api-doc-doxygen
