.. title: Recovering text files with grep
.. slug: recuperando-arquivos-de-texto-com-grep
.. date: 2023-12-15 23:10:09 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text


While I was updating my `resume <https://github.com/padawanphysicist/resume>`_, I deleted the library which generates the pdf version. To my surprise, the file extension was within ``.gitignore`` scope, so the file was not commited and I've lost a day of work. I've explored some ways to recover files, such as `scalpel <https://github.com/sleuthkit/scalpel>`_ and `foremost <https://github.com/mistal-distal/foremost>`_, but none of them worked. After losing the hope, I was amazed to find out that ``grep`` could help me!

.. TEASER_END

When you delete a file, it is not completely removed from the hard drive, even when you are sure that your trash bin has been emptied. The disk space which was used to store the file is simply *deallocated*, meaning the disk space can now be used to store new data whenever you write data on the drive. Before this space is reused, permanently deleted files can indeed be recovered.

Looking at the man page of ``grep`` I found this:

.. image:: /images/Screenshot_20231215_235023.png

which means that devices are read just as if they were ordinary files by default! Then, if you know a piece of text to look for, you can find the file even though it's deleted!
           
I was looking for an Emacs Lisp library file called `ox-altacv.el`, and Emacs libraries usually start with the text

.. code::
   
   ;;; ox-altacv.el blah blah blah

The partition where the deleted file belongs was ``/dev/sdc``. Then, I tried the following command:

.. code:: bash
   
   $ sudo grep --text --after-context=300 --before-context=300 ';;; ox-altacv.el' /dev/sdc > recovery_data

When the command finishes, you will have a (possibly) large file containing all kind of text (ASCII and non ASCII).

I knew there was no non-ASCII text within my file, so it was pointless to keeep the non-ASCII garbage. To cleanup the file, I used the command ``strings``:

.. code:: bash
   
   $ strings recovery_data > data.txt

In the end, ``data.txt`` contained my script but also many other scripts that contain the same line that i was trying to match, but it was easy to find the right one.
