<!--
.. title: Influências
.. slug: influencias_artisticas
.. date: 2023-03-30 00:01:43 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Como artista, você é influenciado pelas coisas. Você é o resultado da combinação de tudo que te influencia. Cada figura representa algo que me inspira: um mapa para o caso de precisar de alguma atualização.

<hr class="my-5 divider mx-auto">

<div id="influencias" class="row grid" `data-masonry`="{percentPosition: true}"></div>
