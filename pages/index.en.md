<!--
.. title: Hi!
.. slug: index
.. date: 2022-06-10 15:37:11 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

My name is Victor. I'm a data scientist/engineer, climber, physicist, artist. My background is in theoretical physics, and my [research](/en/ciencia/) is focused on understanding gravitational phenomena. I currently work on the board of the {{% external_link url=https://observatoriodefortaleza.fortaleza.ce.gov.br/ title="Observatório de Fortaleza" text="Observatório de Fortaleza" %}} of the *Fortaleza Planning Institute*, as a data scientist/engineer.

My art is available under the *creative commons* license; you can support my work through {{% external_link url=https://liberapay.com/padawanphysicist/ title="Liberapay" text=liberapay %}} or {{% external_link url=https://www.paypal.com/donate?hosted_button_id=T4WUREP6AVFRG title="PayPal" text="paypal" %}}.

This page is a place where I can share some of my research, my artwork or anything I find useful on the internet. 

<div id="quantum-cat">
<img src="https://images.procreate.art/images/303966/75741ef4256a32745.jpg" class="mx-auto d-block img-fluid" alt="Quantum Cat" width=300>
</div>
