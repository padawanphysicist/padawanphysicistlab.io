<!-- 
.. title: About
.. slug: sobre
.. date: 2023-03-20 22:35:52 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

I am an enthusiast of the ideas of {{% external_link url=https://www.aguia.usp.br/noticias/ciencia-aberta-e-o-novo-modus-operandi-de-comunicar-pesquisa-parte-i/ title="About open science (pt_br)" text="open science" %}} and {{% external_link url=https://www.gnu.org/philosophy/free-sw.pt-br.html title="Free Software" text="free software" %}}, and I have been trying to apply them in the development of tools that help the process of scientific research. Development takes place primarily on {{% external_link url=https://gitlab.com/padawanphysicist title="GitLab" text="GitLab" %}}.

All processes involving a computer are (generally) done on my laptop, using the operating system {{% external_link url=https://www.gentoo.org/get-started/about/ title="Gentoo Linux" text="Gentoo Linux" %}}, using {{% external_link url=https://xmonad.org/ title="XMonad" text="XMonad" %}} or {{% external_link url=https://kde.org/en-us/plasma-desktop/ title="Plasma" text="Plasma" %}} as desktop environments.




This page was built using the static website generator {{% external_link url=https://getnikola.com/ title="Nikola" text="Nikola" %}}, using the theme {{% external_link url=https://gitlab.com/padawanphysicist/nikola-theme-lightray title="Lightray" text="lightray" %}}, created by me. All pages are edited using {{% external_link url=https://www.gnu.org/software/emacs/ title="GNU Emacs" text="GNU Emacs" %}}.


# Art

Traditional/analog arts are usually made with any material at hand. However, some materials that I always try to carry with me:

- Staedtler Mars Technico 780 2.0 mm 780 C-CA mechanical pencil with HB graphite
- (for drafting) Pentel GraphGear 500 0.3mm, 0.5mm and 0.7mm pencils with B/2B
- lead (for pencil finishing) Unipin technical pen 0.1, 0.5, 0.7 and Brush
- Sketchbook Spiral A6 with Canson paper 100g/m<sup>2</sup>

If the drawing is not finalized on paper, it serves as the basis for the digital version. For this I use {{% external_link url="https://krita.org/en/" title="Krita" text="Krita" %}} on my computer as the main tool, <s>drawing with a graphics tablet {{% external_link url=https://www.wacom.com/pt-br/products/wacom-intuos title="Wacom Intuos S" text="Wacom Intuos S" %}}</s>. Some further image processing is done using {{% external_link url=https://inkscape.org/pt-br/ title="Inkscape" text="Inkscape" %}} or {{% external_link url=https://www.gimp.org/ title="Gimp" text="Gimp" %}}. <s>I recently</s> got an iPad Pro 11, and <s>some of</s> the digital work I've been doing on it as well, using {{% external_link url=https://procreate.com/ title="Procreate" text="Procreate" %}}.

# Science

Since my research is of a theoretical nature, I don't need much more than paper and pen. However, some tools help a lot in manual work:

- {{%external_link url=https://www.sagemath.org/ title="SageMath" text="SageMath" %}} for general purpose math calculations
- {{%external_link url=https://cadabra.science/ title="Cadabra" text="Cadabra" %}} for tensor calculations

