<!-- 
.. title: Arte
.. slug: arte
.. date: 2023-03-30 00:36:54 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

A minha arte é uma forma de me conectar com a intuição, trazendo mais clareza sobre reflexões, desejos e caminhos a explorar. Também é uma maneira de entender mais as ideias de outras pessoas que encontro durante a vida.

Tenho gostado de explorar mais contraste e texturas, especialmente através das hachuras. Você pode ver [aqui](/influencias_artisticas/) minhas principais referências.

Você pode acompanhar nas redes sociais,

<ul class="list-group list-group-horizontal fs-1 justify-content-center">
  <li class="list-group-item border-0">
    <a href="https://www.instagram.com/padawanphysicist/">
      <i class="bi bi-instagram"></i>
    </a>
  </li>
  <li class="list-group-item border-0">
    <a href="https://mastodon.art/@padawanphysicist">
      <i class="bi bi-mastodon"></i>
    </a>
  </li>
  <li class="list-group-item border-0">
    <a href="https://www.pinterest.com/padawanphysicist">
      <i class="bi bi-pinterest"></i>
    </a>
  </li>
</ul>
<p>
 ou conhecer um pouco na galeria a seguir:
</p>

<div id="artwork" class="row grid" `data-masonry`="{percentPosition: true}">
</div>
