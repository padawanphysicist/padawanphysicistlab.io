<!-- 
.. title: Sobre
.. slug: sobre
.. date: 2023-03-20 22:35:52 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Sou um entusiasta das ideias de {{% external_link url=https://www.aguia.usp.br/noticias/ciencia-aberta-e-o-novo-modus-operandi-de-comunicar-pesquisa-parte-i/ title="Sobre ciência aberta" text="ciência aberta" %}} e de {{% external_link url=https://www.gnu.org/philosophy/free-sw.pt-br.html title="Software Livre" text="software livre" %}}, e tenho tentado aplicá-los no desenvolvimento de ferramentas que auxiliem o processo da pesquisa científica. O desenvolvimento se dá principalmente no {{% external_link url=https://gitlab.com/padawanphysicist title="GitLab" text="GitLab" %}}.

Todos os processos que envolvem um computador são (em geral) feitos no meu laptop, utilizando o sistema operacional {{% external_link url=https://www.gentoo.org/get-started/about/ title="Gentoo Linux" text="Gentoo Linux" %}}, usando o {{% external_link url=https://xmonad.org/ title="XMonad" text="XMonad" %}} ou o {{% external_link url=https://kde.org/pt-br/plasma-desktop/ title="Plasma" text="Plasma" %}} como ambientes de trabalho.

Essa página foi construída usando o gerador de sites estáticos {{% external_link url=https://getnikola.com/ title="Nikola" text="Nikola" %}}, usando o tema {{% external_link url=https://gitlab.com/padawanphysicist/nikola-theme-lightray title="Lightray" text="lightray" %}}, criado por mim. Todas as páginas são editadas usando o {{% external_link url=https://www.gnu.org/software/emacs/ title="GNU Emacs" text="GNU Emacs" %}}. 

# Arte

As artes tradicionais/analógicas são geralmente feitas com qualquer material em mãos. Contudo, alguns materiais que tento sempre levar comigo são:

- Lapiseira Staedtler Mars Technico 780 2.0 mm 780 C-CA com grafite HB (para rascunho)
- Lapiseiras Pentel GraphGear 500 0.3mm, 0.5mm e 0.7mm com grafites B/2B (para finalização a lápis)
- Caneta técnica unipin 0.1, 0.5, 0.7 e Brush
- Sketchbook Espiral A6 com papel Canson 100g/m<sup>2</sup>

Se o desenho não for finalizado no papel, ele serve como base para a versão digital. Para isso eu uso no computador o {{% external_link url="https://krita.org/en/" title="Krita" text="Krita" %}} como ferramenta principal, <s>desenhando com uma mesa digitalizadora {{% external_link url=https://www.wacom.com/pt-br/products/wacom-intuos title="Wacom Intuos S" text="Wacom Intuos S" %}}</s>. Algum tratamento posterior na imagem é feito usando o {{% external_link url=https://inkscape.org/pt-br/ title="Inkscape" text="Inkscape" %}} ou o {{% external_link url=https://www.gimp.org/ title="Gimp" text="Gimp" %}}. <s>Recentemente</s>, adquiri um iPad Pro 11, e <s>alguns dos</s> trabalhos digitais tenho feito nele também, usando o {{% external_link url=https://procreate.com/ title="Procreate" text="Procreate" %}}.

# Ciência

Sendo a minha pesquisa de caráter teórico, não é necessário muito além de papel e caneta. Porém, algumas ferramentas ajudam bastante no trabalho braçal:

- {{%external_link url=https://www.sagemath.org/ title="SageMath" text="SageMath" %}} para cálculos matemáticos de propósito geral
- {{%external_link url=https://cadabra.science/ title="Cadabra" text="Cadabra" %}} para cálculos tensoriais


