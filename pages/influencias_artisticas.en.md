<!--
.. title: Influences
.. slug: influencias_artisticas
.. date: 2023-03-30 00:01:43 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

As an artist, you are influenced by things. You are the result of the combination of everything influences you. Each figure represent something that inspires me: a map in case I need some refreshing.

<hr class="my-5 divider mx-auto">

<div id="influencias" class="row grid" `data-masonry`="{percentPosition: true}"></div>
