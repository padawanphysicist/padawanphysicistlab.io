<!-- 
.. title: Science
.. slug: ciencia
.. date: 2023-03-20 22:31:42 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

<blockquote>
<p>
The more you reveal the mystery, two will spring up. (Jorge Mautner)
</p>
</blockquote>

I research a variety of aspects of gravitational interaction in the high curvature regime, to better understand the relationship between gravity and quantum theory (it falls within the field of phenomenology of quantum gravity). In particular, I have worked on the following lines:
- Quasinormal modes of black holes;
- Black hole thermodynamics;
- Violations of Lorentz symmetry.

Currently, working as a data scientist has taken an even closer approach to computational skills, and I've been starting to research in my spare time about numerical analysis as well.

You can take a look at my {{% external_link url=https://gitlab.com/padawanphysicist title="GitLab" text="profile on GitLab" %}} to see ongoing projects, and for a complete list of publications you can take a look at:

- [arXiv](https://arxiv.org/a/0000-0001-6725-2761)
- [inSPIRE](https://inspirehep.net/literature?sort=mostrecent&size=25&page=1&q=a%20V.Santos.1)
- [Google Scholar](https://scholar.google.com.br/citations?user=0FObzQMAAAAJ)

