<!--
.. title: Olá!
.. slug: index
.. date: 2022-06-10 15:37:11 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Meu nome é Victor. Sou cientista/engenheiro de dados, escalador, físico, artista. Minha formação é em física teórica, e a minha [pesquisa](/ciencia/) é voltada para a compreensão de fenômenos gravitacionais. Atualmente trabalho na diretoria do {{% external_link url=https://observatoriodefortaleza.fortaleza.ce.gov.br/ title="Observatório de Fortaleza" text="Observatório de Fortaleza" %}} do *Instituto de Planejamento de Fortaleza*, como cientista e engenheiro de dados.

A minha arte está disponível sob a licença *creative commons*. Você pode apoiar o meu trabalho via {{% external_link url=https://liberapay.com/padawanphysicist/ title="Liberapay" text=liberapay %}} ou {{% external_link url=https://www.paypal.com/donate?hosted_button_id=T4WUREP6AVFRG title="PayPal" text="paypal" %}}.

Esta página é o local onde posso compartilhar parte da minha pesquisa, minhas artes ou qualquer coisa útil que eu encontro pela internet.

<div id="quantum-cat">
<img src="https://images.procreate.art/images/303966/75741ef4256a32745.jpg" class="mx-auto d-block img-fluid" alt="Quantum Cat" width=300>
</div>

