<!-- 
.. title: Ciência
.. slug: ciencia
.. date: 2023-03-20 22:31:42 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

<blockquote>
<p>
Quanto mais você revela o mistério, brotam dois. (Jorge Mautner)
</p>
</blockquote>

Eu pesquiso uma variedade de aspectos da interação gravitacional no regime de alta curvatura, para compreender melhor a relação entre gravidade e a teoria quântica (se enquadra no campo da fenomenologia de gravitação quântica). Em particular, tenho trabalhado nas seguintes linhas: 

- Modos quasinormais de buracos negros;
- Termodinâmica de buracos negros;
- Violações da simetria de Lorentz.

Atualmente, o trabalho como cientista de dados tem feito uma aproximação ainda maior com as habilidades computacionais, e tenho começado a pesquisar no tempo livre sobre análise numérica também.

Você pode checar o meu {{% external_link url=https://gitlab.com/padawanphysicist title="GitLab" text="perfil no GitLab" %}} para ver os projetos em andamento, e para uma lista completa de publicações pode dar uma olhada em:

- [arXiv](https://arxiv.org/a/0000-0001-6725-2761)
- [inSPIRE](https://inspirehep.net/literature?sort=mostrecent&size=25&page=1&q=a%20V.Santos.1)
- [Google Scholar](https://scholar.google.com.br/citations?user=0FObzQMAAAAJ)

