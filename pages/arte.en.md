<!--
.. title: Art
.. slug: arte
.. date: 2023-03-30 00:36:54 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

My art is a way to connect with intuition, bringing more clarity about reflections, desires and paths to explore. It's also a way to understand more of other people's ideas that I encounter in my life.

I've been enjoying exploring more contrast and textures, especially through hatching.

You can follow on social media,
<ul class="list-group list-group-horizontal fs-1 justify-content-center">
  <li class="list-group-item border-0">
    <a href="https://www.instagram.com/padawanphysicist/">
      <i class="bi bi-instagram"></i>
    </a>
  </li>
  <li class="list-group-item border-0">
    <a href="https://mastodon.art/@padawanphysicist">
      <i class="bi bi-mastodon"></i>
    </a>
  </li>
  <li class="list-group-item border-0">
    <a href="https://www.pinterest.com/padawanphysicist">
      <i class="bi bi-pinterest"></i>
    </a>
  </li>
</ul>
<p>
 or find out more in the gallery below:
</p>

<div id="artwork" class="row grid" `data-masonry`="{percentPosition: true}">
</div>
